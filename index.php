<?php
    echo "<h1> TUGAS OOP </h1>";
    require_once('animal.php');
    require_once('ape.php');
    require_once('frog.php');
    $sheep = new Animal("Shaun");
    echo "Nama: ".$sheep->name."<br>"; 
    echo "legs: ".$sheep->legs."<br>";
    echo "cold blooded: ".$sheep->cold_blooded."<br><br>";

    $buduk = new Frog("Buduk");
    echo "Nama: ".$buduk->name."<br>"; 
    echo "legs: ".$buduk->legs."<br>";
    echo "cold blooded: ".$buduk->cold_blooded."<br>";
    $buduk -> jump();
    
    $sungokong = new Ape("Kera Sakti");
    echo "Nama: ".$sungokong->name."<br>"; 
    echo "legs: ".$sungokong->legs."<br>";
    echo "cold blooded: ".$sungokong->cold_blooded."<br>";
    $sungokong -> yell();
?>